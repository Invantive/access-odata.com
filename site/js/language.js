
dictionary = {
    "en": {

        // Error text
        //===============
        "_error_no_info": "Please enter all fields",
        "_error_server_database": "Please enter a server URL and or database",
        "_error_server": "Please enter a server URL",
        "_error_no_db_found": "No database found in URL, please enter one manually",
        "_error_unknown": "An unknown error has occured",
        "_error_unauthorized": "Incorrect user name/password",

        // Loading text
        //===============
        "_load_connecting": "Connecting to server",
        "_load_excel": "Exporting to Excel",
        "_load_csv": "Exporting to CSV",
        "_load_login": "Logging in",

        // General text
        //===============
        "_welcome": "Welcome",
        "_user_name": "User Name",
        "_password": "Password",
        "_server": "Server",
        "_database": "Database",
        "_connect_text": "Connect",
        "_login_text": "Login",
        "_conn_type": "Connection Type",
        "_disconnect": "Disconnect",
        "_export_excel": "Export to Excel",
        "_export_csv": "Export to CSV",
        "_search_term": "Enter your search term",
        "_from": "from"
    },
    "nl": {

        // Error text
        //===============
        "_error_no_info": "Vul alle velden in",
        "_error_server_database": "Voer een server URL in en of een database",
        "_error_server": "Voer een server URL in",
        "_error_no_db_found": "Geen database gevonden uit de URL, vul het database veld in",
        "_error_unknown": "Er is een onbekende fout opgetreden",
        "_error_unauthorized": "Incorrecte gebruikersnaam/wachtwoord",

        // Loading text
        //===============
        "_load_connecting": "Verbinden met server",
        "_load_excel": "Exporteren naar Excel",
        "_load_csv": "Exporteren naar CSV",
        "_load_login": "Inloggen",

        // General text
        //===============
        "_welcome": "Welkom",
        "_user_name": "Gebruikersnaam",
        "_password": "Wachtwoord",
        "_server": "Server",
        "_database": "Database",
        "_connect_text": "Verbinden",
        "_login_text": "Inloggen",
        "_conn_type": "Verbindings Type",
        "_disconnect": "Uitloggen",
        "_export_excel": "Exporteer naar Excel",
        "_export_csv": "Exporteer naar CSV",
        "_search_term": "Zoeken...",
        "_from": "uit"
    }
};

function setLanguage( lan ) {
  if (dictionary.hasOwnProperty(lan)) {
      language = lan;
      set_lang(dictionary[lan]);
      // Hardcoding for now.
      $("#searchInput").attr("placeholder", dictionary[language]["_search_term"]);
  }
}
