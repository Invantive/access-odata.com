
var baseURL = "https://app.office365-apps.com/powerbi/bridge/v11";
var meURL = "https://app.office365-apps.com/powerbi/bridge/v11/me";
var dbURL = "https://app.office365-apps.com/powerbi/bridge/v11/setup/databases";

var dictionary, set_lang, language;

function setupLanguages() {
  "use strict";

  set_lang = function (dictionary) {
      $("[data-translate]").text(function () {
          var key = $(this).data("translate");
          if (dictionary.hasOwnProperty(key)) {
              return dictionary[key];
          }
      });
  };

  setLanguage("en");
}

// On ready
$(document).ready(function() {

  $( "#connectBtn" ).click(function(e) {
    getData();
  });

  hideLoader();

  setupLanguages();

});

$(document).click(function(e){

    // Check if click was triggered on or within menu.
    if( $(e.target).closest(".UserCard").length > 0 ) {
        return false;
    }
    if( $(e.target).closest(".userBarWrapper").length > 0 ) {
        return false;
    }

    closeUserCard();
});

// For in the future, change connection type.
var connType = "IvBridge"

function setConnType( id ) {
  connType = id;
}

function getConnType( id ) {
  return connType;
}

function setErrorStatus( string ) {

  // Translate if possible
  if (dictionary[language][string]) {
      string = dictionary[language][string];
  }

  $(".logonStatusText").text( string );
}

function clearErrorStatus() {
  $(".logonStatusText").text( "" );
}

function isValidOData( usr, pass, srv, db, dUrl ) {
  // Checks if no information is entered at all.
  if ( !usr && !pass && !srv && !db ) {
    setErrorStatus( "_error_no_info" )
    return false;
  }
  else if ( !srv && !db )
  {
    setErrorStatus( "_error_server_database" )
    return false;
  }
  else if ( !srv )
  {
    setErrorStatus( "_error_server" )
    return false;
  }
  else if ( !db )
  {
    setErrorStatus( "_error_no_db_found" )
    return false;
  }
  else
  {
    return true;
  }
}

function closeLogin() {
  $(".loginSection").fadeOut();
  $(".dbSection").delay(100).fadeIn();
  $(".userBarWrapper").delay(100).fadeIn();
}

function openLogin() {
  closeUserCard();
  clearErrorStatus();

  $(".dbSection").fadeOut();
  $(".userBarWrapper").fadeOut();
  $(".loginSection").delay(100).fadeIn();
}

var isUserCardOpen = false;

function openUserCard() {
  $(".UserCard").fadeIn()
  isUserCardOpen = true;
}

function closeUserCard() {
  if ( isUserCardOpen ) {
    $(".UserCard").fadeOut()
    isUserCardOpen = false;
  }
}

var selectedDB = 0;

function setSelectedDB( id ) {
  selectedDB = id;
}

var meEmail,
meName,
meID,
meParty,
meLang,
mePhone,
meAvatarURL,
meAvatarB64,
mePass,
meJob,
meFName;

var databaseList;

function getMeData() {

  var username  = $('#InputUsername').val();
  var password  = $('#InputPassword').val();
  //
  // GET call for oData URL
  //
  var meData = $.ajax({
      type: 'GET'
      , url: meURL
      , datatype: 'json'
      , headers: {
          "Authorization": "Basic " + btoa(username + ":" + password)
      }
      , beforeSend: function() {
        showLoader();
        setLoadText("_load_login");
      }
      , complete: function(){
        hideLoader();
      }
      , success: function (oResult) {
          clearErrorStatus();

          // Store some user values.
          meName        = oResult.givenName;
          meFName       = oResult.name;
          meEmail       = oResult.emailAddress;
          meEmailAudit  = oResult.auditEmailAddress;
          meID          = oResult.cognitoUserId;
          meParty       = oResult.partyName;
          meJob         = oResult.jobTitle;
          meLang        = oResult.languageCode;
          mePhone       = oResult.phoneNumber;
          meAvatarURL   = oResult.userIconUrl;
          meAvatarB64   = oResult.userIcon;
          mePass        = password;

          closeLogin();

          // Set the language and configure user info.
          setLanguage( meLang );
          $(".userNameText").text( ", " + meName );
          $(".userBarWrapper").addClass("show");

          // The info next to the avatar.
          $("#avaUserName").text( meFName );
          $("#avaPartyName").text( meParty );

          $(".userCardInfo1").text( meFName );
          $(".userCardInfo2").text( meParty );

          // If you have an avatar, display it.
          if ( meAvatarURL ) {
            $(".avatarIcon").css("background-image","url('" + meAvatarURL + "')");
            $(".avatarIconBig").css("background-image","url('" + meAvatarURL + "')");
          } else if ( meAvatarB64 ) {
            $(".avatarIcon").css("background-image","url('data:image/png;base64," + meAvatarB64 + "')");
            $(".avatarIconBig").css("background-image","url('data:image/png;base64," + meAvatarB64 + "')");
          }

      }
      , error: function (oResult) {

          var errorText = oResult.statusText;
          if ( errorText == "error" ) {
            setErrorStatus("_error_unknown")
            return;
          }
          if ( errorText == "Unauthorized" ) {
            setErrorStatus("_error_unauthorized")
            return;
          }

          setErrorStatus( "Error: " + errorText )
      }
  });

  var dbData = $.ajax({
      type: 'GET'
      , url: dbURL
      , datatype: 'json'
      , headers: {
          "Authorization": "Basic " + btoa(username + ":" + password)
      }
      , beforeSend: function() {
        showLoader();
        setLoadText("_load_connecting");
      }
      , complete: function(){
        hideLoader();
      }
      , success: function (oResult) {
          clearErrorStatus();

          databaseList = oResult.data;

          $.each(databaseList, function (index, itemData) {
              var id    = itemData.id;
              var name  = itemData.name;
              $(".dbSelection").append("<option value='" + id + "'>" + name + "</option>");
          });

      }
      , error: function (oResult) {

          var errorText = oResult.statusText;
          if ( errorText == "error" ) {
            setErrorStatus("_error_unknown")
            return;
          }
          if ( errorText == "Unauthorized" ) {
            setErrorStatus("_error_unauthorized")
            return;
          }

          setErrorStatus( "Error: " + errorText )
      }
  });
}

var selectedDatabase

function getData() {

    //
    // Create variables
    //
    var username = meID;
    var password = mePass;

    var database = $(".dbSelection").children("option:selected").val();
    var databaseName = $(".dbSelection").children("option:selected").text();

    var server = baseURL + "/" + database + "/odata4/";

    var _name, _n, _nicename, _tableURL;

    //
    // GET call for oData URL
    //
    var data = $.ajax({
        type: 'GET'
        , url: server
        , datatype: 'json'
        , headers: {
            "Authorization": "Basic " + btoa(username + ":" + password)
        }
        , beforeSend: function() {
          showLoader();
          setLoadText("_load_connecting");
        }
        , complete: function(){
          hideLoader();
        }
        , success: function (oResult) {
            clearErrorStatus();

            selectedDatabase = databaseName;

            $('.logonStep').fadeOut();
            $('.exportStep').removeClass('hidden');
            $.each(oResult.value, function (index, itemData) {

                _name = oResult.value[index].name;
                _n = _name.lastIndexOf(".");
                _niceName = decodeURIComponent(_name.substring( _n+1, _name.length ));

                _tableURL = oResult.value[index].url;

                $(".exportStep .contentList ul").append("<li>" + "<input onclick='checkRadioButtons()' name='listitem' id='radio" + [index] + "' type='radio' value='" + _tableURL + "'><label for='radio" + [index] + "'>" + _niceName + "<hr></li>");
                $('input[type=radio]').prop('checked', false)[0].checked = true;
            });
            var tableName = $("input:radio[name=listitem]:checked").val();
            var n = tableName.lastIndexOf(".");
            var niceName = decodeURIComponent(tableName.substring( n+1, tableName.length ));

            $(".tableName").text(niceName);
            $('.DBName').text(selectedDatabase);
            $('.btmDBName').text(" " + selectedDatabase);


        }
        , error: function (oResult) {

            var errorText = oResult.statusText;
            if ( errorText == "error" ) {
              setErrorStatus("_error_unknown")
              return;
            }
            if ( errorText == "Unauthorized" ) {
              setErrorStatus("_error_unauthorized")
              return;
            }

            setErrorStatus( "Error: " + errorText )
        }
    });

    //
    // Export to Excel
    //
    function getTableJsonExcel() {

        showLoader();
        setLoadText("_load_connecting");

        var urlContent = $("input:radio[name=listitem]:checked").val();
        var executeUrl = server + urlContent;
        var dataJSON = $.ajax({
            type: 'GET'
            , url: executeUrl
            , datatype: 'json'
            , headers: {
                "Authorization": "Basic " + btoa(username + ":" + password)
            }
            , beforeSend: function() {
              showLoader();
              setLoadText("_load_excel");
            }
            , complete: function(){
              hideLoader();
            }
            , success: function (oResult) {
                function goExcel() {
                    var createXLSLFormatObj = [];
                    var headerArray = [];
                    /* XLS Head Columns */
                    $.each(oResult.value[0], function (key, value) {
                        headerArray.push(key)
                    });
                    var xlsHeader = headerArray;
                    /* XLS Rows Data */
                    var xlsRows = oResult.value;
                    createXLSLFormatObj.push(xlsHeader);
                    $.each(xlsRows, function (index, value) {
                        var innerRowData = [];
                        $.each(value, function (ind, val) {
                            innerRowData.push(val);
                        });
                        createXLSLFormatObj.push(innerRowData);
                    });
                    /* File Name */
                    var filename = urlContent + ".xlsx";
                    /* Sheet Name */
                    var ws_name = "Results";
                    if (typeof console !== 'undefined');
                    var wb = XLSX.utils.book_new()
                        , ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);
                    /* Add worksheet to workbook */
                    XLSX.utils.book_append_sheet(wb, ws, ws_name);
                    /* Write workbook and Download */
                    if (typeof console !== 'undefined');
                    XLSX.writeFile(wb, filename);
                    if (typeof console !== 'undefined');
                };
                goExcel();
            }
            , error: function (oResult) {
                console.log('Something went wrong');
            }
        });
    }

    function getTableJsonCSV() {
        var urlContent = $("input:radio[name=listitem]:checked").val();
        var executeUrl = server + urlContent;
        var dataJSON = $.ajax({
            type: 'GET'
            , url: executeUrl
            , datatype: 'json'
            , headers: {
                "Authorization": "Basic " + btoa(username + ":" + password)
            }
            , beforeSend: function() {
              showLoader();
              setLoadText("_load_csv");
            }
            , complete: function(){
              hideLoader();
            }
            , success: function (oResult) {
                (function (window) {
                    "use strict";
                    /**
                    Default constructor
                    */
                    var _CSV = function (JSONData) {
                        if (typeof JSONData === 'undefined') return;
                        var csvData = typeof JSONData != 'object' ? JSON.parse(settings.JSONData) : JSONData
                            , csvHeaders, csvEncoding = 'data:text/csv;charset=utf-8,'
                            , csvOutput = ""
                            , csvRows = []
                            , BREAK = '\r\n'
                            , DELIMITER = ','
                            , FILENAME = urlContent + ".csv";
                        // Get and Write the headers
                        csvHeaders = Object.keys(csvData[0]);
                        csvOutput += csvHeaders.join(',') + BREAK;
                        for (var i = 0; i < csvData.length; i++) {
                            var rowElements = [];
                            for (var k = 0; k < csvHeaders.length; k++) {
                                rowElements.push(csvData[i][csvHeaders[k]]);
                            } // Write the row array based on the headers
                            csvRows.push(rowElements.join(DELIMITER));
                        }
                        csvOutput += csvRows.join(BREAK);
                        // Initiate Download
                        var a = document.createElement("a");
                        if (navigator.msSaveBlob) { // IE10
                            navigator.msSaveBlob(new Blob([csvOutput], {
                                type: "text/csv"
                            }), FILENAME);
                        }
                        else if ('download' in a) { //html5 A[download]
                            a.href = csvEncoding + encodeURIComponent(csvOutput);
                            a.download = FILENAME;
                            document.body.appendChild(a);
                            setTimeout(function () {
                                a.click();
                                document.body.removeChild(a);
                            }, 66);
                        }
                        else if (document.execCommand) { // Other version of IE
                            var oWin = window.open("about:blank", "_blank");
                            oWin.document.write(csvOutput);
                            oWin.document.close();
                            oWin.document.execCommand('SaveAs', true, FILENAME);
                            oWin.close();
                        }
                        else {
                            alert("Support for your specific browser hasn't been created yet, please check back later.");
                        }
                    };
                    window.CSVExport = _CSV;
                })(window);
                // From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
                if (!Object.keys) {
                    Object.keys = (function () {
                        'use strict';
                        var hasOwnProperty = Object.prototype.hasOwnProperty
                            , hasDontEnumBug = !({
                                toString: null
                            }).propertyIsEnumerable('toString')
                            , dontEnums = [
          'toString'
          , 'toLocaleString'
          , 'valueOf'
          , 'hasOwnProperty'
          , 'isPrototypeOf'
          , 'propertyIsEnumerable'
          , 'constructor'
        ]
                            , dontEnumsLength = dontEnums.length;
                        return function (obj) {
                            if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
                                throw new TypeError('Object.keys called on non-object');
                            }
                            var result = []
                                , prop, i;
                            for (prop in obj) {
                                if (hasOwnProperty.call(obj, prop)) {
                                    result.push(prop);
                                }
                            }
                            if (hasDontEnumBug) {
                                for (i = 0; i < dontEnumsLength; i++) {
                                    if (hasOwnProperty.call(obj, dontEnums[i])) {
                                        result.push(dontEnums[i]);
                                    }
                                }
                            }
                            return result;
                        };
                    }());
                }
                new CSVExport(oResult.value);
                return false;
            }
            , error: function (oResult) {
                console.log('Something went wrong');
            }
        });
    }
    $(".exportCSV").click(function () {
        getTableJsonCSV();
    });
    $(".exportExcel").click(function () {
        getTableJsonExcel();
    });
    $(".disconnectButton").click(function () {
        $('.logonStep').fadeIn();
        $('.exportStep').addClass('hidden');
        $(".exportStep .contentList ul").empty();
        //$('.logonForm').trigger("reset");
        $(".exportButton").unbind();
    });
}

function checkRadioButtons() {
    var tableName = $("input:radio[name=listitem]:checked").val();
    var n = tableName.lastIndexOf(".");
    var niceName = decodeURIComponent( tableName.substring( n+1, tableName.length ) );
    $(".tableName").text(niceName);
}
//
// Search Function
//
function searchList() {
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('searchInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("contentListItems");
    li = ul.getElementsByTagName('li');
    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        l = li[i].getElementsByTagName("label")[0];
        txtValue = l.textContent || l.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        }
        else {
            li[i].style.display = "none";
        }
    }
}

// Load screen functions.

function setLoadText(msg) {

  // Translate if possible
  if (dictionary[language][msg]) {
      msg = dictionary[language][msg];
  }

  $("#loadText").text(msg);
}

function hideLoader() {
  $(".loaderWrapper").fadeOut();
}

function showLoader() {
  $(".loaderWrapper").fadeIn();
}
